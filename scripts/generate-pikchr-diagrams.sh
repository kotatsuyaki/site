#!/bin/sh

PIKCHR="pikchr"
pikchr_files=$(find . -name '*.pikchr')
for pikchr_file in $pikchr_files; do
    svg_file="${pikchr_file/.pikchr/.svg}"
    pikchr --svg-only "$pikchr_file" > "$svg_file"
    sed -i '2 i <style>text { font-family: sans-serif; }</style>' "$svg_file"
    echo "$svg_file"
done

#+title: About
#+published: 2023-09-08
#+author: kotatsuyaki

I'm kotatsuyaki.  The stuff that I make outside of work ends up here.

* Links
- [[https://codeberg.org/kotatsuyaki/][Codeberg]]
- [[https://fosstodon.org/@kotatsuyaki][Fosstodon]]

* Contact
- Email: ~blog@mail.kotatsu.dev~
- +~Matrix: @kotatsuyaki:matrix.kotatsu.dev~+  The server is currently being re-located, might be months before I find a new place for it.  The email server stays up.

** The services may go down sometimes
Both of them are currently self-hosted, which means that there's a decent chance for my replies to be shoved into the junk folder by the big providers. Check your junk folder. The matrix chat is a fallback in case the email service goes down temporarily.

** You mentioned my stuff?
I try to keep this site simple, and hence it does /not/ come with the IndieWeb goodies like [[https://www.w3.org/TR/webmention/][Webmention]]. If you happen to link to my stuff from your personal site, maybe drop me a line and I may add a backlink below my posts.

* OpenPGP public key
Should you need to send me encrypted messages or verify contents to be made by me:

#+begin_export html
<ul><li><a href="/static/keys/E1FA6B01FBE08775.asc"><code>E1FA6B01FBE08775</code></a> (link to .asc file)</li></ul>
#+end_export

{
  description = "kotatsuyaki's blog";
  nixConfig.bash-prompt = "blog-devshell $ ";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { flake-utils, nixpkgs, self }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        config = { };
        overlays = [ (import ./nix/haskell-overlay.nix) ];
        pkgs = import nixpkgs { inherit config overlays system; };
        packages = with pkgs.myHaskellPackages; { inherit blog-ssg; };
      in
        {
          inherit packages;

          apps.default = flake-utils.lib.mkApp {
            drv = packages.blog-ssg;
            exePath = "/bin/blog-ssg";
          };

          # Default development shell for editing blog posts
          devShells.default = with pkgs; mkShell {
            packages = [
              myHaskellPackages.blog-ssg
              myHaskellPackages.pandoc
              myHaskellPackages.pandoc-crossref
              myHaskellPackages.pandoc-sidenote
              nil
              wrangler
            ];
          };

          # Development shell for editing the site generator
          devShells.ssg = pkgs.myHaskellPackages.shellFor {
            packages = p: [ p.blog-ssg ];

            buildInputs = with pkgs.myHaskellPackages; [
              cabal-install
              ormolu
              haskell-language-server
              pkgs.nil
              ghci

              pandoc-crossref
              pandoc-sidenote
              pkgs.entr
              pkgs.pikchr
            ];

            withHoogle = true;
          };

          # Development shell for those posts that use Python to generate diagrams
          devShells.py = pkgs.mkShell {
            packages = [
              (pkgs.python3.withPackages (ps: with ps; [
                numpy
                scipy
                matplotlib
              ]))
            ];
          };
        }
    );
}

final: prev:
let
  inherit (prev.stdenv) mkDerivation;
  inherit (prev.lib.trivial) flip pipe;
  inherit (prev.lib) throwIf;
  inherit (prev.lib.strings) removePrefix concatStrings;
  inherit (prev.haskell.lib)
    appendPatch
    appendConfigureFlags
    dontCheck
    doJailbreak;

  withPatch = flip appendPatch;
  withFlags = flip appendConfigureFlags;

  ghcVersion = let
    ghcVersion_ = "ghc966";
  in
    throwIf ((removePrefix "ghc" ghcVersion_) !=
      (builtins.replaceStrings [ "." ] [ "" ] prev.haskellPackages.ghc.version))
      (concatStrings [
        "ghcVersion ${ghcVersion_} does not match the default haskellPackages.ghc.version ${prev.haskellPackages.ghc.version} from nixpkgs. "
        "Long build time expected."
      ])
      ghcVersion_;
in
{
  myHaskellPackages = prev.haskell.packages.${ghcVersion}.override {
    overrides = hpFinal: hpPrev: {
      blog-ssg = hpPrev.callCabal2nix "blog-ssg" ../ssg { };
    };
  };
}

#+title: Edit Old Commit with Lazygit
#+author: kotatsuyaki
#+published: 2021-09-12

I used to drop right to the shell whenever I needed to edit an old commit, be it a silly typo or a missing import I caught before pushing to a remote repository. Doing so with the git cli is not hard.

#+begin_src shell
git log # look up the commit hash to be edited
git rebase --interactive 'abcdef^' # caret means its parent
# mark the commit as "edit" and apply changes
git commit --all --amend --no-edit
git rebase --continue
#+end_src

Using [[https://github.com/jesseduffield/lazygit][lazygit]], the process is almost identical.

1. Navigate to the commit to be edited and press =e= (/edit commit/).
2. Stage changes and press =A= (/Amend commit with staged changes/).
3. Press =m= (/view merge/rebase options/) and select =continue=.

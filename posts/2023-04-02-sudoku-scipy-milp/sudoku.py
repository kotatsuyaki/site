from typing import List
from itertools import product
import numpy as np

from sudoku.benchmarks import LeetcodeBench, HardBench, Bench
from sudoku.solutions import IlpSolution, EasyDfsSolution, FastDfsSolution, Solution

if __name__ == '__main__':
    all_benches: List[Bench] = [LeetcodeBench(), HardBench()]
    all_solutions: List[Solution] = [
        IlpSolution(), EasyDfsSolution(), FastDfsSolution()]
    for bench in all_benches:
        all_exec_secs = []
        for solution in all_solutions:
            exec_secs = bench.bench(solution)
            all_exec_secs.append(exec_secs)
        np.save(f'results/{bench.name()}_exec_secs.npy',
                np.array(all_exec_secs))

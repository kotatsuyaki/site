import csv
from pathlib import Path
from time import process_time
from typing import List, cast
from textwrap import wrap

import numpy as np
from numpy import float32
from numpy.typing import NDArray

from .bench import Bench
from ..solutions import Solution


class HardBench(Bench):
    def __init__(self) -> None:
        super().__init__()

        csv_path = Path(__file__).parent / 'HardestDatabase110626.txt'
        print(csv_path)
        with open(csv_path, 'r') as f:
            csv_reader = csv.DictReader(f)
            self.cases = [*csv_reader]

    def bench(self, solution: Solution) -> NDArray[float32]:
        exec_secs = []

        for i, case in enumerate(self.cases):
            board = parse_puzzle_str(case['PuzzleR'])

            start = process_time()
            solution.solveSudoku(board)
            end = process_time()

            exec_secs.append(end - start)
            print(
                f'Solver {solution.__class__} solved {i} in {end - start:.2} seconds')
        return np.array(exec_secs)

    def name(self) -> str:
        return 'hard'


def parse_puzzle_str(puzzle_str: str) -> List[List[str]]:
    splitted_str = wrap(puzzle_str, 9)
    return [*map(lambda s: list(s), splitted_str)]

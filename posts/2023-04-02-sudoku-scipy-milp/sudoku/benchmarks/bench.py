from abc import ABC, abstractmethod

from numpy import float32
from numpy.typing import NDArray

from ..solutions import Solution


class Bench(ABC):
    @abstractmethod
    def bench(self, solution: Solution) -> NDArray[float32]:
        pass

    @abstractmethod
    def name(self) -> str:
        pass

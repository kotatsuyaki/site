from typing import List
from itertools import product
from scipy.optimize import milp, LinearConstraint, Bounds
import numpy as np

from .solution import Solution


class IlpSolution(Solution):
    def solveSudoku(self, board: List[List[str]]) -> None:
        constraints = create_constraints(board)
        milp_output = milp(
            np.zeros(729),
            bounds=Bounds(lb=np.zeros(729), ub=np.ones(729)),
            constraints=constraints,
            # 1 means that the element must be an integer
            integrality=np.full(729, 1),
        )
        return set_board_to_solution(board, milp_output.x)


def set_board_to_solution(board: List[List[str]], solution: np.ndarray):
    for r, c in product(range(9), repeat=2):
        for v in range(9):
            if solution[index(v, r, c)] == 1.0:
                board[r][c] = str(v + 1)
                break


def create_constraints(board: List[List[str]]) -> List[LinearConstraint]:
    rule_constraints = create_rule_constraints()
    instance_constraints = create_instance_constraints(board)
    return rule_constraints + instance_constraints


def create_instance_constraints(board: List[List[str]]) -> List[LinearConstraint]:
    constraints = []
    for r, c in product(range(9), repeat=2):
        ch = board[r][c]
        if ch == '.':
            continue
        v = int(ch) - 1
        A = np.zeros(729)
        A[index(v, r, c)] = 1
        constraints.append(LinearConstraint(A, lb=1, ub=1))

    return constraints


def create_rule_constraints() -> List[LinearConstraint]:
    constraints = []
    for r, c in product(range(9), repeat=2):
        A = np.zeros(729)
        for v in range(9):
            A[index(v, r, c)] = 1
        constraints.append(LinearConstraint(A, lb=1, ub=1))

    for v, r in product(range(9), repeat=2):
        A = np.zeros(729)
        for c in range(9):
            A[index(v, r, c)] = 1
        constraints.append(LinearConstraint(A, lb=1, ub=1))

    for v, c in product(range(9), repeat=2):
        A = np.zeros(729)
        for r in range(9):
            A[index(v, r, c)] = 1
        constraints.append(LinearConstraint(A, lb=1, ub=1))

    for x, y, v in product([0, 3, 6], [0, 3, 6], range(9)):
        A = np.zeros(729)
        for r, c in product(range(x, x + 3), range(y, y + 3)):
            A[index(v, r, c)] = 1
        constraints.append(LinearConstraint(A, lb=1, ub=1))

    return constraints


def index(v: int, r: int, c: int) -> int:
    return v * 81 + r * 9 + c

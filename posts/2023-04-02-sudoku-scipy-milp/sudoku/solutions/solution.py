from abc import ABC, abstractmethod
from typing import List


class Solution(ABC):
    @abstractmethod
    def solveSudoku(self, board: List[List[str]]) -> None:
        """Solve the Sudoku puzzle in `board` in-place"""
        pass

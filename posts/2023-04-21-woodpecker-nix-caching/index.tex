% Created 2023-08-08 Tue 21:06
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{kotatsuyaki}
\date{\today}
\title{Locally Cached Nix CI with Woodpecker}
\hypersetup{
 pdfauthor={kotatsuyaki},
 pdftitle={Locally Cached Nix CI with Woodpecker},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 28.2 (Org mode 9.7-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Motivation}
\label{sec:orgec11d0b}
I've been migrating my repositories off from GitHub to \href{https://codeberg.org}{Codeberg} for public repositories under free licenses, and to my own instance of \href{https://forgejo.org}{Forgejo} for private or non-free repositories.  One crucial functionality that I needed was a CI/CD system.  While Codeberg provides access to its experimentally hosted instance of \href{https://woodpecker-ci.org}{Woodpecker CI} on \href{https://codeberg.org/Codeberg-CI/request-access}{a per-user basis},
I don't feel like taking advantage of their public service to deploy this blog and some other shady personal projects.

As Woodpecker can be self hosted, the obvious solution is to run my own instance of it, which led to the next problem -- Downloading and building the same set of dependencies for each job repeatedly wastes bandwidth and computation.  Although I didn't care about resource usage at all when exploiting GitHub free-tier offering, I do care about it now that the jobs are running on my own hardware.

bIt turned out that there's a blog post by Kevin (Kevin, 2022) describing how to cache the Nix store for GitLab Runner. I adapted their approach to Woodpecker, hence this post.

\section{Assumptions}
\label{sec:orge1b0790}
\begin{itemize}
\item NixOS machine(s).
\item A domain name.
\item Some familiarity with Nix.  This post does not hand-hold you through the detailed process.
\end{itemize}

\section{Brief Steps}
\label{sec:org3878416}
\begin{enumerate}
\item Create an OAuth2 application on Codeberg (or any Git hosting service of your choice).
On Codeberg, this is done at \url{https://codeberg.org/user/settings/applications}.
The "Redirect URI" should be \texttt{https://your.woodpecker.domain/authorize}.
Save the client ID and client secret for later use.

\item Generate an "agent secret" by this command.
This is a pre-shared secret for communication between the Woodpecker server and the agents (i.e. runners).

\begin{verbatim}
   openssl rand -hex 32
\end{verbatim}

\item Setup the Woodpecker server,
which is responsible for communicating with the Git host,
serving the web UI,
and dispatching jobs to Woodpecker agents (i.e. runners).

The server configuration environment variables are listed on the [Server configuration][doc-server-config] doc page.
A bare minimum config should contain these variables:

\begin{itemize}
\item \texttt{WOODPECKER\_ADMIN}
\item \texttt{WOODPECKER\_HOST}
\item \texttt{WOODPECKER\_GITEA}
\item \texttt{WOODPECKER\_GITEA\_URL}
\item \texttt{WOODPECKER\_GRPC\_ADDR}
\item \texttt{WOODPECKER\_SERVER\_ADDR}
\item \texttt{WOODPECKER\_GITEA\_CLIENT} (from step 1)
\item \texttt{WOODPECKER\_GITEA\_SECRET} (from step 1)
\item \texttt{WOODPECKER\_AGENT\_SECRET} (from step 2)
\end{itemize}

My server configuration \href{https://codeberg.org/kotatsuyaki/server-nixos-config/src/commit/218b0ef3df750b389b5fcb5bd78f9d0a151dc43f/cibox/application.nix\#L93}{is in this file}.
Do note that my setup has a Nginx proxy in front of the Woodpecker server.
The proxy is optional, and you can have Woodpecker handle HTTPS on its own by setting \texttt{WOODPECKER\_LETS\_ENCRYPT=true}.

\item Setup the Woodpecker agent.

The agent configuration environment variables are listed on the \href{https://woodpecker-ci.org/docs/administration/agent-config}{Agent configuration} doc page.
A bare minimum config should contain these variables:

\begin{itemize}
\item \texttt{WOODPECKER\_SERVER}
\item \texttt{WOODPECKER\_AGENT\_SECRET}
\end{itemize}

Additionally, to use the Docker-compatible API provided by Podman,

\begin{verbatim}
   WOODPECKER_BACKEND=docker
   DOCKER_HOST=unix:///run/podman/podman.sock
\end{verbatim}

\begin{note}
\textbf{Rootful Docker has trouble with Nix's \texttt{-{}-{}store} option}

I chose to use Podman specifically due to previous difficulties getting rootful Docker work with Nix's \texttt{-{}-{}store} option.  Your mileage may vary, but I met with this error message.

\begin{verbatim}
   error: setting up a private mount namespace: Operation not permitted
\end{verbatim}
\end{note}

\begin{note}
\textbf{Podman DNS in Nixpkgs}

It's required to set the following options when using Podman from Nixpkgs. Otherwise, the CI jobs in containers can't resolve domain names and thus fail to clone Git repositories\footnote{See discussions on \url{https://discourse.nixos.org/t/podman-containers-dns/26820/4}}.

\begin{verbatim}
   virtualisation.podman.defaultNetwork.settings.dns_enable = true;
   networking.firewall.interfaces."podman+" = {
     allowedUDPPorts = [ 53 ];
     allowedTCPPorts = [ 53 ];
   };
\end{verbatim}
\end{note}

My agent configuration \href{https://codeberg.org/kotatsuyaki/server-nixos-config/src/commit/218b0ef3df750b389b5fcb5bd78f9d0a151dc43f/cibox/application.nix\#L93}{is in this file}.

\item Test the setup by creating a repository with a \texttt{.woodpecker.yml} file, enabling and trusting the repository in Woodpecker's web UI, and push.
The YAML file should look like this.
Replace \texttt{greet} with the name of the flake output to be executed.

\begin{verbatim}
   pipeline:
     run-greet:
       image: nixos/nix
       commands:
         - echo 'experimental-features = flakes nix-command' >> /etc/nix/nix.conf
         - nix run --store unix:///mnt/nix/var/nix/daemon-socket/socket?root=/mnt .#greet -L
       volumes:
         - /nix:/mnt/nix:ro
\end{verbatim}

The approach of mounting \texttt{/nix} into \texttt{/mnt/nix} and using the Unix socket (\texttt{-{}-{}store unix:///...}) to communicate with the host's Nix daemon is credit to the post by Kevin (Kevin, 2022).

Try running the same build multiple times. You should be able observe subsequent jobs running much faster thanks to Nix's caching. For example,
\label{orgaf5efc7} shows the later build of this blog being much faster than its previous build.
\end{enumerate}


\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./subsequent-jobs.png}
\caption{\label{fig:org7e77efe}A screenshot of two Woodpecker build jobs of this blog}
\end{figure}

\begin{note}
\textbf{Exposing the host's Nix store}

Exposing the host's Nix store directly to the containers has some security implications.
The Nix store is world-readable, so doing so gives the CI jobs the ability to read any sensitive data, if present in the Nix store.

If this is a concern for you, Kevin's post has some other options that doesn't bind-mount the host's Nix store.
\end{note}
\end{document}
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes" doctype-system="about:legacy-compat" />
  <xsl:template match="/">
    <html lang="en">
      <link rel="stylesheet" href="/static/style.css" />
      <head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title><xsl:value-of select="atom:feed/atom:title" /></title>
      </head>
      <body>
	<header>
	  <h1 id="index-header-h1">
            <a href="/">kotatsuyaki’s site</a>
	  </h1>
	</header>
	<main>
          <h2>
            Web feed preview
          </h2>
          <aside class="note">
            <h3>What is this?</h3>
            <p>
              This is the preview of a <strong>web feed</strong>.
	      To subscribe, <strong>copy the URL from the address bar into your newsreader</strong>.
            </p>

            <p>
              Visit <a href="https://aboutfeeds.com">About Feeds</a> to get started with newsreaders and subscribing.
              It's free in charge, free as in freedom, and anonymous.
            </p>
          </aside>
          <h3>
            <xsl:value-of select="atom:feed/atom:title" />
          </h3>
          <p>
            <xsl:variable name="description" select="atom:feed/atom:subtitle" />
            <xsl:value-of select="$description" />
            <ul class="post-list">
	      <xsl:apply-templates select="atom:feed/atom:entry" />
	    </ul>
          </p>
	</main>
        <nav>
          <ul>
            <li><a href="/">Home</a></li>
          </ul>
        </nav>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="atom:entry">
    <li class="post-list-item">
      <a>
	<xsl:attribute name="href">
	  <xsl:value-of select="atom:id" />
	</xsl:attribute>
	<xsl:value-of select="atom:title" />
      </a>
      <span class="post-list-item-published">
	<xsl:variable name="published" select="atom:published" />
	<!-- Remove the time and timezone parts from the RFC-3339 dates -->
	<xsl:value-of select="substring-before($published, 'T')" />
      </span>
    </li>
  </xsl:template>
</xsl:stylesheet>

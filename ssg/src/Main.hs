-- For string literal patterns
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import Control.Applicative
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Ord
import qualified Data.Map.Strict as Map
import Data.Text (pack, unpack, Text, stripSuffix)
import Debug.Trace
import Hakyll
import Hakyll.Core.Compiler.Internal
import Text.Printf (printf)
import Text.Pandoc (Pandoc, Block(Header, CodeBlock, RawBlock), Inline(Link, Image), ReaderOptions, WriterOptions)
import Text.Pandoc.Walk
import Text.Pandoc.Options
import Data.Org (org, orgMeta, OrgFile)
import Path (Path, parseRelFile, parent, (</>))
import qualified GHC.IO.Encoding as Encoding
import qualified Path

pathToFilePath = Path.toFilePath

siteRoot = "https://blog.kotatsu.dev"

main :: IO ()
main = do
  -- Fix runtime errors on non-UTF-8 systems
  Encoding.setLocaleEncoding Encoding.utf8
  hakyll $ do
    match "posts/*/index.org" $ do
      compile $ contentPageCompiler True
      route $ setExtension "html"

    match "notes/*/index.org" $ do
      compile $ contentPageCompiler True
      route $ setExtension "html"

    match "pages/*/index.org" $ do
      compile $ contentPageCompiler False
      route $ setExtension "html"

    create ["feed.xml"] $ do
      route idRoute
      compile $ do
        let feedCtx = postCtx `mappend` bodyField "description"
        posts <- loadAllSnapshots "posts/*/index.org" "body"
        let sortedPosts = sortOn (Down . itemIdentifier) posts
        let identifiers = itemIdentifier <$> sortedPosts
        meta <- gatherMetadata identifiers True
        atomTemplate <- loadBody "templates/atom.xml"
        itemTemplate <- loadBody "templates/atom-item.xml"
        renderAtomWithTemplates
          atomTemplate itemTemplate
          mainFeedConfiguration (meta `mappend` feedCtx) sortedPosts

    create ["notes.xml"] $ do
      route idRoute
      compile $ do
        let feedCtx = postCtx `mappend` bodyField "description"
        notes <- loadAllSnapshots "notes/*/index.org" "body"
        let sortedNotes = sortOn (Down . itemIdentifier) notes
        let identifiers = itemIdentifier <$> sortedNotes
        meta <- gatherMetadata identifiers True
        atomTemplate <- loadBody "templates/atom.xml"
        itemTemplate <- loadBody "templates/atom-item.xml"
        renderAtomWithTemplates
          atomTemplate itemTemplate
          notesFeedConfiguration (meta `mappend` feedCtx) sortedNotes

    create ["notes/index.html"] $ do
      route idRoute
      compile $ do
        notes <- loadAllSnapshots "notes/*/index.org" "body"
        let sortedNotes = sortOn (Down . itemIdentifier) notes
        let identifiers = itemIdentifier <$> notes
        meta <- gatherMetadata identifiers False
        notesTemplate <- loadBody "templates/notes.html"
        let notesCtx = listField "notes" meta (return sortedNotes)
                       `mappend` constField "root" siteRoot
        makeItem "" >>= applyTemplate notesTemplate notesCtx

    create ["index.html"] $ do
      route idRoute
      compile $ do
        posts <- loadAllSnapshots "posts/*/index.org" "body"
        -- Dirty sort by path name
        -- The path names are expected to be prefixed with their published dates
        let sortedPosts = sortOn (Down . itemIdentifier) posts
        let identifiers = itemIdentifier <$> posts
        meta <- gatherMetadata identifiers False
        indexTemplate <- loadBody "templates/index.html"
        let indexCtx = listField "posts" meta (return sortedPosts)
                       `mappend` constField "root" siteRoot
        makeItem "" >>= applyTemplate indexTemplate indexCtx

    create ["sitemap.xml"] $ do
      route idRoute
      compile $ do
        posts <- loadAllSnapshots "posts/*/index.org" "body"
        notes <- loadAllSnapshots "notes/*/index.org" "body"
        -- Dirty sort by path name
        -- The path names are expected to be prefixed with their published dates
        let postIdentifiers = itemIdentifier <$> posts
        postMeta <- gatherMetadata postIdentifiers False

        let noteIdentifiers = itemIdentifier <$> notes
        noteMeta <- gatherMetadata noteIdentifiers False

        sitemapTemplate <- loadBody "templates/sitemap.xml"
        let sitemapCtx =
              listField "posts" (postMeta `mappend` constField "root" siteRoot) (return posts)
              `mappend` listField "notes" (noteMeta `mappend` constField "root" siteRoot) (return notes)
              `mappend` constField "root" siteRoot
        makeItem "" >>= applyTemplate sitemapTemplate sitemapCtx

    -- Compile citations
    match "posts/*/*.bib" $ compile biblioCompiler

    match "posts/*/*.csl" $ compile cslCompiler

    -- Copy static files
    match "static/**" $ do
      compile copyFileCompiler
      route idRoute

    match "robots.txt" $ do
      compile copyFileCompiler
      route idRoute

    match "_headers" $ do
      compile copyFileCompiler
      route idRoute

    create ["404.html"] $ do
      compile $ do
        notFoundTemplate <- loadBody "templates/404.html"
        makeItem "" >>= applyTemplate notFoundTemplate (defaultContext
                                                        `mappend` constField "root" siteRoot)
      route idRoute

    -- Compile templates
    match "templates/*" $ compile templateBodyCompiler

    match allPostImagePattern $ do
      compile copyFileCompiler
      route idRoute

contentPageCompiler :: Bool -> Compiler (Item String)
contentPageCompiler isListed = do
  -- Information used throughout the do-block
  postIdent <- getUnderlying
  postBody <- getResourceBody
  -- The base (relative) path of the org source file
  postFileBase <- getPostFileBase <$> getUnderlying

  -- Extract and optionaly save metadata from org file
  metadataList <- getResourceBody >>= orgMetadataFromItem >>= addCleanUrlMetadata
  when isListed $ void (saveSnapshot "meta" metadataList)

  -- Add implicit dependency from the .org source and its image assets
  imageDependency <- makePatternDependency $ getPostImagePattern postFileBase
  compilerTellDependencies [imageDependency]

  -- Only process bibliography when both files specified in metadata
  let bibPaths = getRelBibPaths (itemBody metadataList) postFileBase
  processBib <- processBibAtPath bibPaths

  let postCtx = mconcat (uncurry constField <$> itemBody metadataList)
        `mappend` constField "root" siteRoot
        `mappend` defaultContext

  -- The body template renders intermediate content, used in both the feeds and the web page
  bodyTemplate <- loadBody "templates/body.html"
  -- The post template renders the web page, and it depends on the output of the body template
  postTemplate <- loadBody "templates/post.html"

  -- Render and save the body for generating the feeds
  renderedRawBody <- getResourceBody
    >>= renderPandocWithTransformIM
    defaultHakyllReaderOptions
    postHtmlWriterOptions
    (transformPostCompiler processBib (postRawCompiler postFileBase))
  renderedBody <- applyTemplate bodyTemplate postCtx renderedRawBody
  saveSnapshot "body" renderedBody

  -- Render the post web page
  applyTemplate postTemplate postCtx renderedRawBody

processBibAtPath :: Maybe (String, String) -> Compiler (Item Pandoc -> Compiler (Item Pandoc))
processBibAtPath (Just (cslPath, bibPath)) = do
  csl <- load (fromFilePath cslPath)
  bib <- load (fromFilePath bibPath)
  return $ processPandocBiblio csl bib
processBibAtPath Nothing = return return

-- | Get the post transform by supplying an itemCompiler and a rawCompiler
transformPostCompiler :: (Item Pandoc -> Compiler (Item Pandoc)) -> (Pandoc -> Compiler Pandoc) -> Item Pandoc -> Compiler (Item Pandoc)
transformPostCompiler itemCompiler rawCompiler doc = do
  doc <- itemCompiler doc
  flip withItemBody doc $ do rawCompiler

postRawCompiler :: PostBasePath -> Pandoc -> Compiler Pandoc
postRawCompiler postFileBase doc = do
  return . rerootPostRelativeUrl postFileBase . wrapCollapseInDetails . legalizeCodeBlockLanguage . lowerHeadings $ doc

renderPandocWithTransformIM
  :: ReaderOptions -> WriterOptions
  -> (Item Pandoc -> Compiler (Item Pandoc))
  -> Item String
  -> Compiler (Item String)
renderPandocWithTransformIM ropt wopt f i =
  writePandocWith wopt <$> (f =<< readPandocWith ropt i)

orgMetadataFromItem :: Item String -> Compiler (Item [(String, String)])
orgMetadataFromItem = withItemBody $
  return . fmap stringPairFromTextPair. Map.toList . orgMeta . fromJust . org . pack
    where stringPairFromTextPair (k, v) = (unpack k, unpack v)

postCtx :: Context String
postCtx =
    dateField "date" "%Y-%m-%d" `mappend`
    defaultContext

-- | Remove all text after the first occurence of the metadata marker
getStringBeforeMarker :: Item String -> Compiler (Item String)
getStringBeforeMarker = withItemBody $ return . head . splitOn metadataMarker

metadataMarker = "# END_METADATA\n"

-- | Strip out the leading "#+" prefixes from Org metadata fields
stripLeadingPoundPlusses :: Item String -> Compiler (Item String)
stripLeadingPoundPlusses = withItemBody $ return . join . intersperse "\n" . Data.List.map stripLeadingPoundPlus . lines

stripLeadingPoundPlus :: String -> String
stripLeadingPoundPlus = fromMaybe "" . stripPrefix "#+"

-- | Lower (increase, actually) header levels so that h1 is reserved for the post title
lowerHeadings :: Pandoc -> Pandoc
lowerHeadings = walk lowerHeading
  where
    lowerHeading (Header level attr inlines) = Header (level + 1) attr inlines
    lowerHeading others = others


wrapCollapseInDetails :: Pandoc -> Pandoc
wrapCollapseInDetails = walk $ concatMap wrap
  where
    wrap cb@(CodeBlock (ident, classes, attrs) text) =
      if ("collapse", "yes") `elem` attrs
        then [RawBlock "html5" "<details><summary>Code Listing</summary>",
              cb,
              RawBlock "html5" "</details>"]
        else [cb]
    wrap x = [x]

-- | Transform code block language tags ("classes") for Pandoc
--
-- Org-mode uses the major mode names minus the "-mode" suffix, whereas pandoc uses names from the skylighting library.
-- Code blocks with non-existent syntax names are rendered differently, which is undesirable.
-- This function tries to map whatever names I use to whatever names pandoc likes.
legalizeCodeBlockLanguage :: Pandoc -> Pandoc
legalizeCodeBlockLanguage = walk legalize
  where
    legalize (CodeBlock (ident, [], attrs) text) = CodeBlock (ident, ["default"], attrs) text
    legalize (CodeBlock (ident, langList, attrs) text) = CodeBlock (ident, mapLang <$> langList, attrs) text
    legalize x = x
    -- Map special cases manually, and leave it unaltered otherwise
    mapLang "shell" = "bash"
    mapLang "text" = "default"
    mapLang "pikchr" = "default"
    mapLang x = x

-- | Transform relative links like ./post-specific-image.png to /2000-01-01-title/post-specific-image.png
rerootPostRelativeUrl :: Path.Path Path.Rel Path.Dir -> Pandoc -> Pandoc
rerootPostRelativeUrl postFileBase = walk rerootInline
  where
     -- The inline mapping function
     rerootInline (Link attr inlines (url, title)) = Link attr inlines (pack . rerootUrl . unpack $ url, title)
     rerootInline (Image attr inlines (url, title)) = Image attr inlines (pack . rerootUrl . unpack $ url, title)
     rerootInline x = x

     -- Only operate on links starting with "./"
     rerootUrl url | "./" `isPrefixOf` url = trace ("URL " ++ url ++ " is relative") (reroot url)
                   | otherwise = trace ("URL " ++ url ++ " is absolute") url

     -- Concat the leading "/" with the post base directory (relative) and the original directory (relative)
     reroot url = ("/" ++) . fromJust $ (relFileString <|> relDirString)
       where
         relFileString = Path.toFilePath . (postFileBase </>) <$> Path.parseRelFile url
         relDirString = Path.toFilePath . (postFileBase </>) <$> Path.parseRelDir url


postHtmlWriterOptions :: WriterOptions
postHtmlWriterOptions = defaultHakyllWriterOptions
                        { writerHTMLMathMethod = MathML
                        }

type PostBasePath = Path.Path Path.Rel Path.Dir

getPostFileBase :: Identifier -> PostBasePath
getPostFileBase = parent . fromJust . parseRelFile . toFilePath

getPostImagePattern :: PostBasePath -> Pattern
getPostImagePattern basePath =
  foldl1 (.||.) . fmap globPatternFromSuffix $ postImageSuffixes
  where
    globPatternFromSuffix = fromGlob . prefixWithBasePath
    prefixWithBasePath = ((pathToFilePath basePath ++ "*.") ++)

allPostImagePattern :: Pattern
allPostImagePattern = foldl1 (.||.) . fmap globPatternFromSuffix $ postImageSuffixes
  where
    globPatternFromSuffix = fromGlob . prefixWithBasePath
    prefixWithBasePath = ("posts/*/*." ++)

postImageSuffixes = ["png", "svg", "jpg", "jpeg", "webp"]

-- | Get relative paths to csl and bib files, relative to the root of the project
getRelBibPaths :: [(String, String)] -> PostBasePath -> Maybe (FilePath, FilePath)
getRelBibPaths metadataList postFileBase = case (cslPath, bibPath) of
                (Just x, Just y) -> Just (x, y)
                _ -> Nothing
              where
                cslPath = lookupRelPathFromPost "csl"
                bibPath = lookupRelPathFromPost "bibliography"
                lookupRelPathFromPost key = pathToFilePath . (postFileBase </>) . fromJust . parseRelFile
                        <$> lookup key metadataList

mainFeedConfiguration :: FeedConfiguration
mainFeedConfiguration = FeedConfiguration
    { feedTitle       = "Posts on kotatsuyaki’s site"
    , feedDescription = "My main feed for long-ish blog posts."
    , feedAuthorName  = "kotatsuyaki"
    , feedAuthorEmail = "blog@mail.kotatsu.dev"
    , feedRoot        = siteRoot
    }

notesFeedConfiguration :: FeedConfiguration
notesFeedConfiguration = FeedConfiguration
    { feedTitle       = "Notes on kotatsuyaki’s site"
    , feedDescription = "My “notes” feed for notes and microblog entries."
    , feedAuthorName  = "kotatsuyaki"
    , feedAuthorEmail = "blog@mail.kotatsu.dev"
    , feedRoot        = siteRoot
    }

-- Get a `Context` that maps
gatherMetadata :: [Identifier] ->
                  Bool ->
                  Compiler (Context String)
gatherMetadata identifiers shouldMapRfcDatetime = do
    -- For each identifier, get a context with its metadata taken from the "meta" snapshot.
    let listCompilerContextString = identToCompilerContextString <$> identifiers
    -- Turn the list of compilers to a compiler of list.
    let compilerListContextString = sequence listCompilerContextString
    -- Concat the list in the compiler into a single context.
    mconcat <$> compilerListContextString
  where
    -- From a single identifier, get its meta snapshot and convert all the metadata into a
    -- single context that returns an associated value iff the following are all true:
    -- 1) The identifier is matched.
    -- 2) The key is found in the meta snapshot's metadata list
    identToCompilerContextString :: Identifier -> Compiler (Context String)
    identToCompilerContextString ident = do
      itemListStringString <- loadSnapshot ident "meta" >>= mapDatetimeIfRequired :: Compiler (Item [(String, String)])
      let listStringString = itemBody itemListStringString
      let listContextString = fmap (stringStringToContextString ident) listStringString
      return $ mconcat listContextString

    mapDatetimeIfRequired :: Item [(String, String)] -> Compiler (Item [(String, String)])
    mapDatetimeIfRequired = if shouldMapRfcDatetime
      then rfcMapPublishedMetadata
      else return

    -- From an identifier and a single pair of metadata, construct a context that
    -- returns the value iff the key and the identifier matched
    stringStringToContextString :: Identifier -> (String, String) -> Context String
    stringStringToContextString ident (k, v) = field k (itemToV ident v)

    -- Build a context matching function from an identifier and a value.
    -- The key matching is handled by hakyll's field function, thus it's not here.
    itemToV :: Identifier -> String -> Item String -> Compiler String
    itemToV ident v item = do
      let itemIdent = itemIdentifier item
      if ident == itemIdent
        then return v
        else fail ""

-- Add a metadata with the key "url" and with its value being the original but without .html suffix
addCleanUrlMetadata :: Item [(String, String)] -> Compiler (Item [(String, String)])
addCleanUrlMetadata metaList = do
    postIdent <- getUnderlying
    let cleanUrl = ("/" ++) . pathToFilePath . parent . fromJust . parseRelFile . toFilePath $ postIdent
    return $ fmap (++ [("url", cleanUrl)]) metaList

-- Append suffix to make published date rfc-3339 compliant.
-- This is required for the atom feed.
rfcMapPublishedMetadata :: Item [(String, String)] -> Compiler (Item [(String, String)])
rfcMapPublishedMetadata metaList = do
  let metaList' = itemBody metaList
  let metaPublished = fromJust . lookup "published" $ metaList'
  let metaPublished' = metaPublished ++ "T00:00:00Z"
  let metaUpdated = lookup "updated" $ metaList'
  let metaUpdated' = case metaUpdated of
                       Just updated -> updated ++ "T00:00:00Z"
                       Nothing -> metaPublished'
  return $ fmap ([("published", metaPublished'), ("updated", metaUpdated')] ++) metaList
